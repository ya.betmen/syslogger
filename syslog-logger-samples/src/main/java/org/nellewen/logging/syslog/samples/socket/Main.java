package org.nellewen.logging.syslog.samples.socket;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.Properties;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import java.util.logging.SimpleFormatter;
import org.nellewen.logging.syslog.SyslogHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author petr
 */
public class Main {

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS.%1$tL %2$s%n%4$s: %5$s%6$s%n");
    }
    private static Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws IOException, URISyntaxException {
        LogManager logManager = LogManager.getLogManager();
        java.util.logging.Logger logger = logManager.getLogger("");
        Handler[] handlers = logger.getHandlers();
        for (Handler handler : handlers) {
            logger.removeHandler(handler);
            System.out.println("h=" + handler);
        }
        Properties p = new Properties();
        p.setProperty(SyslogHandler.SYSLOG_PROPERTY_URI, "unix:///dev/log");
        StringWriter stringWriter = new StringWriter();
        p.store(stringWriter, null);
        logManager.readConfiguration(new ByteArrayInputStream(stringWriter.getBuffer().toString().getBytes("UTF-8")));

        SyslogHandler syslogHandler = new SyslogHandler();
        SimpleFormatter simpleFormatter = new SimpleFormatter();
        syslogHandler.setFormatter(simpleFormatter);
        logger.addHandler(syslogHandler);

        for (int i = 0; i < 3; i++) {
            LOG.info("test {} - {}", "test" + System.currentTimeMillis(), "test" + i);
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
