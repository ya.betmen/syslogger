package org.nellewen.logging.syslog.sender;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author Petr Zaliautdinov <petr.zalyautdinov@gmail.com>
 */
public class TcpDataSender extends AbstractDataSender {

    protected final String syslogHost;
    protected final int port;
    protected final long timeout;
    protected Socket socket;
    protected DataOutputStream dataOutputStream;

    public TcpDataSender(String syslogHost, String host, int port, String applicationName, long timeout, int maxMessageSize) {
        super(host, applicationName, maxMessageSize);
        this.syslogHost = syslogHost;
        this.port = port;
        this.timeout = timeout;
    }

    @Override
    protected void send(byte[] buffer) {
        int position = buffer.length;
        try {
            if (socket == null) {
                socket = new Socket(syslogHost, port);
                dataOutputStream = new DataOutputStream(socket.getOutputStream());
            }
            dataOutputStream.write(buffer, 0, buffer.length);
            dataOutputStream.write('\n');
            dataOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
            releaseResources();
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException ie) {
                return;
            }
        } catch (Exception t) {
            t.printStackTrace();
            releaseResources();
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    private void releaseResources() {
        try {
            if (socket != null) {
                socket.close();
            }
        } catch (Throwable t) {
            t.printStackTrace();
            socket = null;
        }
    }
}
