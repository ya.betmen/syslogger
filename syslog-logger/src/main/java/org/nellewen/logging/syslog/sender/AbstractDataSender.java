package org.nellewen.logging.syslog.sender;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.nellewen.logging.syslog.SyslogRecord;

/**
 *
 * @author Petr Zaliautdinov <petr.zalyautdinov@gmail.com>
 */
public abstract class AbstractDataSender {

    protected static final String SYSLOG_VERSION = "1";
    protected static final int SYSLOG_TAG_MAX_LENGHT = 32;
    protected static final ZoneId DEFAULT_TIME_ZONE_ID = TimeZone.getDefault().toZoneId();

    private static final SyslogRecord FINISH_RECORD = new SyslogRecord(null, null, 0, 0, 0, null, "");

    protected final BlockingQueue<SyslogRecord> logBuffer = new LinkedBlockingQueue<>();
    protected volatile boolean stoped = false;
    protected final String currentHost;
    protected final String applicationName;
    protected final int maxMessageLength;
    protected final Sender sender;
    protected final Thread senderThread;
    protected final Runnable onShutdown = new Runnable() {
        @Override
        public void run() {
            stoped = true;
        }
    };

    public AbstractDataSender(String currentHost, String applicationName, int maxMessageLength) {
        this.currentHost = currentHost;
        this.applicationName = applicationName;
        this.maxMessageLength = maxMessageLength;
        this.sender = new Sender(maxMessageLength);
        this.senderThread = new Thread(sender);
        this.senderThread.setDaemon(true);
        Runtime.getRuntime().addShutdownHook(new Thread(onShutdown));
    }

    public void append(SyslogRecord logRecord) {
        if (!stoped) {
            logBuffer.add(logRecord);
        }
    }

    public void start() {
        this.senderThread.start();
    }

    public void stop() {
        stoped = true;
        logBuffer.add(FINISH_RECORD);
    }

    public void convert(SyslogRecord logRecord, DateTimeFormatter dtf, StringBuilder sb) {
        int facilityCode = logRecord.getFacility().getCode();
        int severityCode = logRecord.getSeverity().getCode();
        int code = (facilityCode << 3) + severityCode;
        OffsetDateTime atOffset = Instant.ofEpochMilli(logRecord.getTimestamp()).atZone(DEFAULT_TIME_ZONE_ID).toOffsetDateTime();
        String timestamp = atOffset.format(dtf);

        sb.append("<");
        sb.append(code);
        sb.append(">");
        sb.append(SYSLOG_VERSION);
        sb.append(" ");
        sb.append(timestamp);
        sb.append(" ");
        sb.append(currentHost);
        sb.append(" ");
        sb.append(applicationName);
        sb.append(" ");
        sb.append(logRecord.getThreadID());
        sb.append(" ");
        String source = logRecord.getSource();
        int sourceLenght = source.length();
        if (sourceLenght <= SYSLOG_TAG_MAX_LENGHT) {
            sb.append(source);
        } else {
            sb.append(source.charAt(0));
            sb.append("…");
            sb.append(source, sourceLenght - SYSLOG_TAG_MAX_LENGHT, sourceLenght);
        }
        sb.append(" ");
        sb.append(logRecord.getMessage());

        int messageID = logRecord.getMessageID();
        if (messageID > -1) {
            String msgId = Integer.toString(messageID & 0xffffffff, Character.MAX_RADIX);
            sb.append(" #");
            sb.append(msgId);
        }
    }

    protected abstract void send(byte[] logRecord);

    public class Sender implements Runnable {

        private final DateTimeFormatter dtf = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
        private final StringBuilder sb;

        public Sender(int maxMessageLength) {
            sb = new StringBuilder();
        }

        @Override
        public void run() {
            while (!stoped) {
                try {
                    SyslogRecord record = logBuffer.take();
                    if (record != FINISH_RECORD) {
                        sb.setLength(0);
                        convert(record, dtf, sb);
                        ByteBuffer bb = ByteBuffer.allocate(maxMessageLength);
                        byte[] data = sb.toString().getBytes("UTF-8");
                        bb.put(data, 0, Math.min(data.length, maxMessageLength));

                        bb = ByteBuffer.allocate(data.length);
                        bb.put(data);
                        send(data);
                    }
                } catch (InterruptedException | UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                }

            }
        }
    }

}
