package org.nellewen.logging.syslog;

import java.util.logging.Level;

/**
 *
 * @author Petr Zaliautdinov <petr.zalyautdinov@gmail.com>
 */
public enum Severity {
    EMERGENCY(0),
    ALERT(1),
    CRITICAL(2),
    ERROR(3),
    WARNING(4),
    NOTICE(5),
    INFORMATIONAL(6),
    DEBUG(7);

    private final int code;

    private Severity(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static Severity getByCode(int code) {
        Severity result = null;
        if (0 <= code && code <= 7) {
            Severity[] values = values();
            result = values[code];
        }
        return result;
    }

    public static Severity getByJUL(Level level) {
        if (level.intValue() >= Level.SEVERE.intValue()) {
            return Severity.ERROR;
        } else if (level.intValue() >= Level.WARNING.intValue()) {
            return Severity.WARNING;
        } else if (level.intValue() >= Level.INFO.intValue()) {
            return Severity.INFORMATIONAL;
        } else {
            return Severity.DEBUG;
        }
    }
}
