package org.nellewen.logging.syslog;

import java.util.Date;

/**
 *
 * @author Petr Zaliautdinov <petr.zalyautdinov@gmail.com>
 */
public class SyslogRecord {

    private final Facility facility;
    private final Severity severity;
    private final long timestamp;
    private final int threadID;
    private final int messageID;
    private final String source;
    private final String message;

    public SyslogRecord(Facility facility, Severity severity, long timestamp, int threadID, int messageID, String source, String message) {
        this.facility = facility;
        this.severity = severity;
        this.timestamp = timestamp;
        this.threadID = threadID;
        this.messageID = messageID;
        this.source = source;
        char[] value = message.toCharArray();
        for (int i = 0; i < value.length; i++) {
            if (value[i] == 10) {
                value[i] = '\\';
            }
        }
        this.message = new String(value);
    }

    public Facility getFacility() {
        return facility;
    }

    public Severity getSeverity() {
        return severity;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public int getThreadID() {
        return threadID;
    }

    public String getSource() {
        return source;
    }

    public int getMessageID() {
        return messageID;
    }

}
