package org.nellewen.logging.syslog.sender;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.time.format.DateTimeFormatter;
import jnr.unixsocket.UnixDatagramChannel;
import jnr.unixsocket.UnixSocketAddress;
import org.nellewen.logging.syslog.SyslogRecord;

/**
 *
 * @author Petr Zaliautdinov <petr.zalyautdinov@gmail.com>
 */
public class SocketDataSender extends AbstractDataSender {

    private final String socketPath;
    private final long timeout;
    private UnixDatagramChannel channel;
    private UnixSocketAddress address;

    public SocketDataSender(String socketPath, String currentHost, String applicationName, long timeout, int maxMessageSize) {
        super(currentHost, applicationName, maxMessageSize);
        this.socketPath = socketPath;
        this.timeout = timeout;
    }

    @Override
    public void convert(SyslogRecord logRecord, DateTimeFormatter dtf, StringBuilder sb) {
        int facilityCode = logRecord.getFacility().getCode();
        int severityCode = logRecord.getSeverity().getCode();
        int code = (facilityCode << 3) + severityCode;
        sb.append("<");
        sb.append(code);
        sb.append(">");
        sb.append(" ");
        sb.append(logRecord.getMessage());
        int messageID = logRecord.getMessageID();
        if (messageID > -1) {
            String msgId = Integer.toString(messageID & 0xffffffff, Character.MAX_RADIX);
            sb.append(" #");
            sb.append(msgId);
        }
    }

    @Override
    protected void send(byte[] buffer) {
        try {
            if (channel == null) {
                address = new UnixSocketAddress(socketPath);
                channel = UnixDatagramChannel.open();
            }
            channel.send(ByteBuffer.wrap(buffer), address);
        } catch (IOException e) {
            releaseResources();
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException ie) {
                return;
            }
        } catch (Exception t) {
            releaseResources();
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    private void releaseResources() {
        try {
            if (channel != null) {
                channel.close();
            }
        } catch (Throwable t) {
            t.printStackTrace();
            channel = null;
        }
    }
}
