package org.nellewen.logging.syslog;

/**
 *
 * @author Petr Zaliautdinov <petr.zalyautdinov@gmail.com>
 */
public enum Transport {
    UDP,
    TCP,
    UNIX;

    public static Transport getByName(String name) {
        Transport result = null;
        if (name != null) {
            try {
                result = Transport.valueOf(name.toUpperCase());
            } catch (IllegalArgumentException e) {
                //do nothing, just no constant with the specified name
            }
        }
        return result;
    }
}
