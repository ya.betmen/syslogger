package org.nellewen.logging.syslog;

import org.nellewen.logging.syslog.sender.SocketDataSender;
import org.nellewen.logging.syslog.sender.AbstractDataSender;
import org.nellewen.logging.syslog.sender.TcpDataSender;
import org.nellewen.logging.syslog.sender.UdpDataSender;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.CodeSource;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;

/**
 *
 * @author Petr Zaliautdinov <petr.zalyautdinov@gmail.com>
 */
public class SyslogHandler extends Handler {

    public static final String SYSLOG_PROPERTY_URI = SyslogHandler.class.getName() + ".uri";
    public static final String SYSLOG_PROPERTY_MAX_MESSAGE_SIZE = SyslogHandler.class.getName() + ".maxSize";
    public static final String SYSLOG_PROPERTY_APPNAME = SyslogHandler.class.getName() + ".app";
    public static final String SYSLOG_PROPERTY_ADD_MESSAGE_NUMBER = SyslogHandler.class.getName() + ".addMessageNumber";

    public static final int SYSLOG_DEFAULT_MAX_MESSAGE_SIZE = 1024;
    public static final String SYSLOG_DEFAULT_URI = "unix://dev/log";
    public static final int SYSLOG_DEFAULT_PORT = 514;
    public static final long SENDER_DEFAULT_SLEEP_TIMEOUT = 1000;

    private static final AtomicInteger IDGEN = new AtomicInteger(0);

    private final String host;
    private final int port;
    private final String socketPath;
    private final Facility facility;
    private final Transport transport;
    private final String applicationName;
    private final boolean addMessageNumber;
    private final AtomicBoolean inited = new AtomicBoolean(false);

    private AbstractDataSender dataSender;

    public SyslogHandler() throws URISyntaxException {
        String syslogUri = LogManager.getLogManager().getProperty(SYSLOG_PROPERTY_URI);
        String msgMaxSize = LogManager.getLogManager().getProperty(SYSLOG_PROPERTY_MAX_MESSAGE_SIZE);
        String appName = LogManager.getLogManager().getProperty(SYSLOG_PROPERTY_APPNAME);
        String addNumber = LogManager.getLogManager().getProperty(SYSLOG_PROPERTY_ADD_MESSAGE_NUMBER);
        facility = Facility.USER_LEVEL_MESSAGES;
        if (syslogUri == null) {
            syslogUri = SYSLOG_DEFAULT_URI;
        }
        addMessageNumber = Boolean.parseBoolean(addNumber);
        int maxMessageSize = SYSLOG_DEFAULT_MAX_MESSAGE_SIZE;
        if (msgMaxSize != null) {
            try {
                maxMessageSize = Integer.parseInt(msgMaxSize);
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
            }
        }
        URI uri = new URI(syslogUri);
        transport = Transport.getByName(uri.getScheme());
        String currentHost;
        try {
            currentHost = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            currentHost = "localhost";
            ex.printStackTrace();
        }
        String sysloHost = null;
        if (transport == Transport.UNIX) {
            socketPath = uri.getPath();
            port = 0;
        } else {
            socketPath = null;
            sysloHost = uri.getHost();
            port = (uri.getPort() > -1 ? uri.getPort() : SYSLOG_DEFAULT_PORT);
        }
        if (appName == null) {
            CodeSource codeSource = SyslogHandler.class.getProtectionDomain().getCodeSource();
            URL location = codeSource.getLocation();
            URI toURI = location.toURI();
            String path = toURI.getPath();
            appName = new java.io.File(path).getName();
        }
        applicationName = appName;
        host = sysloHost;
        switch (transport) {
            case TCP:
                dataSender = new TcpDataSender(host, currentHost, port, applicationName, SENDER_DEFAULT_SLEEP_TIMEOUT, maxMessageSize);
                break;
            case UDP:
                dataSender = new UdpDataSender(host, currentHost, port, applicationName, SENDER_DEFAULT_SLEEP_TIMEOUT, maxMessageSize);
                break;
            case UNIX:
                dataSender = new SocketDataSender(socketPath, currentHost, applicationName, SENDER_DEFAULT_SLEEP_TIMEOUT, maxMessageSize);
        }

        dataSender.start();
    }

    public AtomicBoolean getInited() {
        return inited;
    }

    @Override
    public void publish(LogRecord record) {
        Formatter formatter = getFormatter();
        String message = formatter.format(record);
        Severity severity = Severity.getByJUL(record.getLevel());
        String source = record.getSourceClassName() + "." + record.getSourceMethodName();
        int id;
        if (addMessageNumber) {
            id = IDGEN.updateAndGet((i) -> {
                return i < 0 ? 0 : i + 1;
            });
        } else {
            id = -1;
        }
        SyslogRecord syslogRecord = new SyslogRecord(facility, severity, record.getMillis(), record.getThreadID(), id, source, message);
        dataSender.append(syslogRecord);
    }

    @Override
    public void flush() {
        //do nothing
    }

    @Override
    public void close() throws SecurityException {
        dataSender.stop();
    }

}
