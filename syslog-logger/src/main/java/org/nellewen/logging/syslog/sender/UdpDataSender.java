package org.nellewen.logging.syslog.sender;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 *
 * @author Petr Zaliautdinov <petr.zalyautdinov@gmail.com>
 */
public class UdpDataSender extends AbstractDataSender {

    protected final String syslogHost;
    protected final int port;
    protected final long timeout;
    protected DatagramSocket socket;

    public UdpDataSender(String syslogHost, String host, int port, String applicationName, long timeout, int maxMessageSize) {
        super(host, applicationName, maxMessageSize);
        this.syslogHost = syslogHost;
        this.port = port;
        this.timeout = timeout;
    }

    @Override
    protected void send(byte[] buffer) {
        int position = buffer.length;
        try {
            if (socket == null) {
                socket = new DatagramSocket();
            }
            socket.send(new DatagramPacket(buffer, buffer.length, InetAddress.getByName(syslogHost), port));
        } catch (IOException e) {
            e.printStackTrace();
            releaseResources();
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException ie) {
                return;
            }
        } catch (Exception t) {
            t.printStackTrace();
            releaseResources();
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    private void releaseResources() {
        try {
            if (socket != null) {
                socket.close();
            }
        } catch (Throwable t) {
            t.printStackTrace();
            socket = null;
        }
    }
}
