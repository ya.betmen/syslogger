package org.nellewen.logging.syslog;

/**
 *
 * @author Petr Zaliautdinov <petr.zalyautdinov@gmail.com>
 */
public enum Facility {

    KERNEL_MESSAGES(0),
    USER_LEVEL_MESSAGES(1),
    MAIL_SYSTEM(2),
    SYSTEM_DAEMONS(3),
    SECURITY_OR_AUTHORIZATION_MESSAGES_1(4),
    MESSAGES_GENERATED_INTERNALLY_BY_SYSLOGD(5),
    LINE_PRINTER_SUBSYSTEM(6),
    NETWORK_NEWS_SUBSYSTEM(7),
    UUCP_SUBSYSTEM(8),
    CLOCK_DAEMON_1(9),
    SECURITY_OR_AUTHORIZATION_MESSAGES_2(10),
    FTP_DAEMON(11),
    NTP_SUBSYSTEM(12),
    LOG_AUDIT(13),
    LOG_ALERT(14),
    CLOCK_DAEMON_2(15),
    LOCAL_USE_0(16),
    LOCAL_USE_1(17),
    LOCAL_USE_2(18),
    LOCAL_USE_3(19),
    LOCAL_USE_4(20),
    LOCAL_USE_5(21),
    LOCAL_USE_6(22),
    LOCAL_USE_7(23);

    private final int code;

    private Facility(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static Facility getByCode(int code) {
        Facility result = null;
        if (0 <= code && code <= 23) {
            Facility[] values = values();
            result = values[code];
        }
        return result;
    }

}
